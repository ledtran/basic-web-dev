// var list = [];
// function getText() {
    // var str = document.getElementById("newTask");

    // list.push(str.value);
    // str.value = "";
    // str.focus();
    // var area = document.getElementById("txtArea");
    // area.value = "";
    // for (var i = 0; i < list.length; i++) {
        // area.value += list[i] + "\n";
    // }
// }

//document.getElementById("add").onclick = function() {
    // First things first, we need our text:
//    var text = document.getElementById("idea").value; //.value gets input values

    // Now construct a quick list element
//    var li = "<li>" + text + "</li>";

    // Now use appendChild and add it to the list!
//    document.getElementById("list").appendChild(li);
//}


//application modules
var app = angular.module('app', []);

//controllers
app.controller('demoController', function($scope) {
    // initial items
    $scope.items = [
    ];

    //*******BUG******* can't add same thing twice???
    //feature inherent to angular
    // add an item
    $scope.add = function() {
      $scope.items.push({val:$scope.input, done:false});
      //$scope.items.push(angular.copy($scope.input))
      $scope.input = '';
    };
    
    //only 
    $scope.markAll = function(allChecked) {
    	//$scope.items.splice(index, 1);
      for(var i = 0; i < $scope.items.length; i++){
        //$scope.items[i].done=true;
        $scope.items[i].done=allChecked;
      }
    };
    
    $scope.numLeft = function(){
      var x = 0;
      for(var j = 0; j < $scope.items.length; j++){
        if($scope.items[j].done==false){
          x++;
        }
      }
      return x;
    }
    
    $scope.clearItems = function(){
      for(var k = 0; k < $scope.items.length; k++){
        if($scope.items[k].done==true){
          $scope.items.splice(k, 1);
          //need to decrement k b/c splice shifts the index
          k--;
        }
      }
    }
    
    $scope.showClear = function(){
      for(var l = 0; l < $scope.items.length; l++){
        if($scope.items[l].done==true){
          return true;
        }
      }
      return false;
    }
    
    
});
